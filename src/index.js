import Phaser from 'phaser';
import BootScene from './scenes/BootScene';
import StartMenuScene from './scenes/StartMenuScene';
import GameScene from './scenes/GameScene';

export const config = {
  title: 'broughlike_tutorial',
  type: Phaser.AUTO,
  scale: {
    mode: Phaser.Scale.FIT,
    parent: 'phaser-example',
    width: 1366,
    height: 768,
  },
  scene: [
    BootScene,
    GameScene,
    StartMenuScene,
  ],
};

const game = new Phaser.Game(config);
export default game;
